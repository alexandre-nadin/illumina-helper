from samplesheet.metadata_reader import MetadataReader
from samplesheet.csv_manipulator import *

__author__ = 'alexandre-nadin'

CSV_DELIMITER = ','

def convert_to_casava(csv_content, fcid=None, ref=None, control=None, recipe=None, operator=None, delimiter=CSV_DELIMITER):
    """
    Converts any csv-formatted sample sheet into the casava format.

    :param csv_content:
    :return:
    """
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)
    """ Set column names """
    for field_tup in CASAVA_ORDER:
        matching_fields = [name for name in field_tup if name in field_names]
        if matching_fields.__len__() > 0:
            """ Rename by default even if it matches """
            csv_content = rename_column(csv_content, matching_fields[0], field_tup[V_CASAVA], delimiter=delimiter)
        else:
            csv_content = add_column(csv_content, field_tup[V_CASAVA], delimiter=delimiter)


    """ Fill columns """
    csv_content = csv_content if fcid is None else fill_column_with_str(csv_content, colname=FCID[V_CASAVA], value=fcid, delimiter=delimiter)
    csv_content = csv_content if ref is None else fill_column_with_str(csv_content, colname=SAMPLE_REF[V_CASAVA], value=ref, delimiter=delimiter)
    csv_content = csv_content if control is None else fill_column_with_str(csv_content, colname=CONTROL[V_CASAVA], value=control, delimiter=delimiter)
    csv_content = csv_content if recipe is None else fill_column_with_str(csv_content, colname=RECIPE[V_CASAVA], value=recipe, delimiter=delimiter)
    csv_content = csv_content if operator is None else fill_column_with_str(csv_content, colname=OPERATOR[V_CASAVA], value=operator, delimiter=delimiter)

    """ Merge indexes """
    csv_content = merge_columns(csv_content, sep='-', end_column=INDEX_1[V_CASAVA], delete_columns=False
                                , columns=[INDEX_1[V_CASAVA], INDEX_2[V_CASAVA]], delimiter=delimiter)

    """ Rearrange column order """
    csv_content = arrange_col_order(csv_content, [colname[V_CASAVA] for colname in CASAVA_ORDER], delimiter=delimiter)
    return csv_content

# class SampleSheetFields:
#     """
#
#     """

""" Versions """
V_NEW, V_CASAVA = 0, 1

""" Fields """
FCID = (None, 'FCID')
LANE = ('Lane', 'Lane')
SAMPLE_ID = ('Sample_ID', 'SampleID')
SAMPLE_NAME = ('Sample_Name', None)
SAMPLE_REF = (None, 'SampleRef')
SAMPLE_PRJ = ('Sample_Project', 'SampleProject')
SAMPLE_PLATE = ['Sample_Plate', None]
SAMPLE_WELL = ['Sample_Well', None]
DESCRIPTION = ['Description', 'Description']
CONTROL = [None, 'Control']
RECIPE = [None, 'Recipe']
OPERATOR = [None, 'Operator']

INDEX_1 = ['index', 'Index_1']
INDEX_2 = ['index2', 'Index_2']
INDEX_I5 = ['I5_Index_ID', None]
INDEX_I7 = ['I7_Index_ID', None]

CASAVA_ORDER = (
    FCID, LANE, SAMPLE_ID, SAMPLE_REF, INDEX_1, INDEX_2, DESCRIPTION, CONTROL, RECIPE, OPERATOR, SAMPLE_PRJ
)

NEW_ORDER = (
    LANE, SAMPLE_ID, SAMPLE_NAME, SAMPLE_PLATE, SAMPLE_WELL, INDEX_I7, INDEX_1, INDEX_I5, INDEX_2, SAMPLE_PRJ
    , DESCRIPTION
)

class SampleSheet:

    SAMPLE_SHEET_NAME = "SampleSheet"
    SAMPLE_SHEET_EXTENSION = ".csv"
    SAMPLE_SHEET_SEPARATOR = ","
    DATA_HEADER = "Data"


    def __init__(self, file_name, separator=',', oldModel=False):
        self._oldModel = oldModel
        self.sampleSheetPath = file_name
        self.validSampleSheet = True
        self.separator = separator
        self._data_header = ""  # Contains the header as a string
        self._data_header_fields = []  # Contains each field in the data header
        self._data_rows = []  # Contains the data of each sample as strings
        self._data_rows_fields = []  # Contains each field in each data row.
        self._rowErrors = []
        self._headerErrors = []

        data = []
        mdr = MetadataReader(file_name)
        if mdr.hasMetadata():
            data = mdr.getRawDataFromMd(self.DATA_HEADER)
        else:
            with open(file_name, 'rU') as file:
                for line in file:
                    data.append(line)
        self.parse_data(data)


    def parse_data(self, data):
        """
        Parse an array of data. Assumes there is NO metadata.

        :param data: The csv reader containing only the data fields and its raw data.
        :return:
        """
        if data.__len__() < 2:
            self._rowErrors.append("There is no data in sample sheet.")
            self.validSampleSheet = False
        else:
            for row in data:
                """ Get all the fields in a line and strips them. """
                fields = map(str.strip, row.split(self.SAMPLE_SHEET_SEPARATOR))

                """ Ignore empty lines """
                if fields.__len__() == 0 or (fields.__len__() == 1 and len(str(fields[0]).strip()) == 0):
                    continue

                """ Initiate the data header. """
                if len(self._data_header) == 0:
                    """ Check there are no empty field """
                    empty_fields = 0
                    for field in fields:
                        if str(field).strip(" ").__len__() == 0:
                            self.validSampleSheet = False
                            empty_fields += 1

                    """ Check if empty fields. Continue parsing anyway. """
                    if empty_fields > 0:
                        self._headerErrors.append("The header has %d empty field(s)." %(empty_fields))
                        self.validSampleSheet = False
                    self._data_header = self.SAMPLE_SHEET_SEPARATOR.join(fields)
                    self._data_header_fields = fields
                else:
                    """ Check if number of fields correspond """
                    data_row = self.SAMPLE_SHEET_SEPARATOR.join(fields)
                    if self._data_header_fields.__len__() == fields.__len__():
                        self._data_rows.append(data_row)
                        self._data_rows_fields.append(fields)
                    else:
                        self.validSampleSheet = False
                        self._rowErrors.append(data_row)


    def is_valid_sample_sheet(self):
        return self.validSampleSheet


    def print_data(self):
        """
        Prints the header and its data.
        """
        print "\n[HEADER]\n." + self._data_header + "."
        print "\n[ROWS]"
        for row in self._data_rows:
            print "." + row + "."

    def get_summary(self):
        summary = []
        if self._data_header.__len__() == 0:
            summary.append("\n[WARNING] No data found")
        if self.is_valid_sample_sheet():
            summary.append("\n[SAMPLE SHEET VALIDATED] Number of lines found: {}".format(self._data_rows.__len__()))
        else:
            summary.append("\n[INVALID SAMPLE SHEET]")
            if self._headerErrors.__len__() > 0:
                summary.append("\nThere are errors with the header: ")
                for e in self._headerErrors:
                    summary.append("\t* {}".format(e))
            if self._rowErrors.__len__() > 0:
                summary.append("\nThose lines don't have the same number of field as the header: ")
                for e in self._rowErrors:
                    summary.append("\n\t* {} ({}/{}).".format(e, len(str(e).split(self.separator))
                                                              , self._data_header_fields.__len__()))

        return summary

    def print_summary(self):
        if self._data_header.__len__() == 0:
            print "\n[WARNING] No data found"
        if self.is_valid_sample_sheet():
            print "\n[SAMPLE SHEET VALIDATED] Number of lines found: ", self._data_rows.__len__()
        else:
            print "\n[INVALID SAMPLE SHEET]"
            if self._headerErrors.__len__() > 0:
                print "There are errors with the header: "
                for e in self._headerErrors:
                    print "\t* ", e
            if self._rowErrors.__len__() > 0:
                print "Those lines don't have the same number of field as the header: "
                for e in self._rowErrors:
                    print "\t* ", e, "(", len(str(e).split(self.separator)), "/", self._data_header_fields.__len__(), ")"


    def get_data_header(self):
        return self._data_header


    def get_data_header_fields(self):
        return self._data_header_fields


    def get_data_rows(self):
        return self._data_rows


    def get_data_rows_fields(self):
        return self._data_rows_fields

    def get_sample_sheet_content(self):
        content = self._data_header
        for row in self._data_rows:
            content += os.linesep + row
        return content

def runTests(argv=None):

    # path = "/Users/alexandre-nadin/dev/illumina-project-restructor/tests/lustre1/SampleSheets/"
    path = os.environ["DEV"] + "/tests/sample-sheets/"

    fileNames = [
        "test-no-metadata.csv"
        , "test-with-metadata-end.csv"
        , "test-with-metadata-between.csv"
        , "test-with-metadata-no-data.csv"
        , "test-with-metadata-between-emtpy.csv"
        , "old-with-empty-line-n-spaces.csv"
        , "new-with-empty-line-n-spaces.csv"
        , "new-with-missing-fields.csv"
        , "SampleSheet.csv"

    ]

    sheet = SampleSheet(path + fileNames[6])
    # sheet.print_data()
    # sheet.print_summary()

    nlines = 2
    sheet = SampleSheet(path + fileNames[-1])
    scontent = sheet.get_sample_sheet_content()
    print "\n[SampleSheet]\n", os.linesep.join(scontent.splitlines())

    print "\n[Convert to Casava]\n", os.linesep.join(convert_to_casava(scontent).splitlines())


def main(argv=None):
    runTests(argv)
    # test(path + "151104_SN859_0242_AHTKFYADXX_IEM.csv")


if __name__ == "__main__":
    print "[SampleSheet] main function"
    sys.exit(main(sys.argv))
