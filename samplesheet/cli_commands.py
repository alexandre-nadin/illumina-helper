import click
from csv_manipulator import *
from sample_sheet import convert_to_casava, SampleSheet

# ------------
# Decorators
# ------------

# TESTING BLOCK  TO REMOVE LATER
# Maybe the input option should be left as '-i', instead of trying to manage a long option.
def set_commandx(func):
    """
    Retrieves function's docstring.
    Sets input if available in stdin.
    """
    cmd_tag = "cmd_"
    cmd_split = func.func_name.split(cmd_tag)
    cmd_name = cmd_split[1] if cmd_split.__len__() > 1 else func.func_name

    def get_doc(func_name, cut_params=True):
        """
        Fetches a function's doc string.
        :param func_name: Name of the function.
        :return: The content of the doc string without the parameters.
        """
        docstring = eval(func_name).__doc__
        return docstring.split(":param")[0] if cut_params and docstring is not None else docstring

    @click.command(help=get_doc(cmd_name, cut_params=True))
    @click.option('-i', '--input', help="Input. Readable from stdin.")
    @click.option('-d', '--delimiter', help="CSV delimiter", default=',')
    def newfunc(*args, **kwargs):
        print "kwargs: " + kwargs.__str__()
        print "args: .{}.".format(args)
        kwiname = ''
        if not sys.stdin.isatty():
            kwargs[kwiname] = ''.join(sys.stdin.readlines())
            # kwargs['i'] = ''.join(sys.stdin.readlines())
            # i = ''.join(sys.stdin.readlines())
        else:
            if kwargs[kwiname] is None:
                kwargs[kwiname] = " "
                # i = " "
        print "args: ", args
        print "kwargs: ", kwargs.__str__()
        print "cmd: " + cmd_name
        # sys.stdout.writelines(func(*args, **kwargs))
        sys.stdout.writelines(eval(cmd_name(*args, **kwargs)))
    return newfunc


def set_command(func):
    """
    Retrieves function's docstring.
    Sets input if available in stdin.
    """
    cmd_tag = "cmd_"
    cmd_split = func.func_name.split(cmd_tag)
    cmd_name = cmd_split[1] if cmd_split.__len__() > 1 else func.func_name

    def get_doc(func_name, cut_params=True):
        """
        Fetches a function's doc string.
        :param func_name: Name of the function.
        :return: The content of the doc string without the parameters.
        """
        docstring = eval(func_name).__doc__
        return docstring.split(":param")[0] if cut_params and docstring is not None else docstring

    @click.command(help=get_doc(cmd_name, cut_params=True))
    @click.option('-i', help="Input. Readable from stdin.")
    @click.option('-d', '--delimiter', help="CSV delimiter", default=',')
    def newfunc(*args, **kwargs):
        if not sys.stdin.isatty():
            """ Stdin is not empty, we read from it """
            kwargs['i'] = ''.join(sys.stdin.readlines())
        else:
            """ stdin is empty, we read from the option """
            if kwargs['i'] is None:
                kwargs['i'] = " "
        sys.stdout.writelines(func(*args, **kwargs))
    return newfunc


# ----------
# Commands
# ----------
@click.option('--fcid')
@click.option('--ref')
@click.option('--control')
@click.option('--recipe')
@click.option('--operator')
@set_command
def cmd_convert_to_casava(i, delimiter, fcid=None, ref=None, control=None, recipe=None, operator=None):
    sheet = SampleSheet(str(i).strip(), separator=delimiter)
    if not sheet.is_valid_sample_sheet():
        sys.stderr.writelines(sheet.get_summary() + ["\n"])
    scontent = sheet.get_sample_sheet_content()
    return convert_to_casava(scontent, fcid=fcid, ref=ref, control=control, recipe=recipe, operator=operator, delimiter=delimiter)


@click.argument('value')
@click.argument('colname', nargs=1)
@click.option('--colfilter', help="Filter by column-values. JSON formatted: '{\"colname_1\": [\"value1\", \"value2\"], \"colname_1\": [\"value1\"]}'")
@set_command
def cmd_fill_column_with_str(i, colname, value, colfilter, delimiter):
    import json
    from StringIO import StringIO
    try:
        colfilter = json.load(StringIO(colfilter)) if colfilter else {}  # json.load needs an IO object with read attribute.
    except ValueError as ve:
        sys.stderr.writelines("value error: " + ve.message)
        colfilter = {}

    return fill_column_with_str(i, colname, value=value, col_filters=colfilter, delimiter=delimiter)


@click.argument('stop', type=click.INT)
@click.argument('colname', nargs=1)
@click.option('--colfilter', help="Filter by column-values. JSON formatted: '{\"colname_1\": [\"value1\", \"value2\"], \"colname_1\": [\"value1\"]}'")
@click.option('--repeat/--no-repeat', default=False)
@click.option('--step', default=1)
@click.option('--start', default=0)
@set_command
def cmd_fill_column_with_serial(i, colname, stop, start, step, repeat, colfilter, delimiter):
    import json
    from StringIO import StringIO

    try:
        colfilter = json.load(StringIO(colfilter)) if colfilter else {}  # json.load needs an IO object with read attribute.
    except ValueError as ve:
        sys.stderr.writelines("value error: " + ve.message)
        colfilter = {}
    return fill_column_with_serial(i, colname, stop+1, start=start, step=step, repeat=repeat, col_filters=colfilter, delimiter=delimiter)


@click.argument('times', default=1)
@click.option('--colfilter', help="Filter by column-values. JSON formatted: '{\"colname_1\": [\"value1\", \"value2\"], \"colname_1\": [\"value1\"]}'", required=True)
@set_command
def cmd_duplicate_lines(i, times, colfilter, delimiter):
    import json
    try:
        return duplicate_lines(i, times=times, where=json.loads(colfilter), delimiter=delimiter)
    except ValueError as ve:
        sys.stderr.writelines("value error: " + ve.message)
        return i


@click.argument('colname', nargs=-1)
@click.option('--reverse/--noreverse', help="Reverses the order.", default=False)
@set_command
def cmd_sort_by_cols(i, colname, reverse, delimiter):
    return sort_by_cols(i, colnames=colname, reverse=reverse, delimiter=delimiter)


@click.option('--sep', help="Separator between contents to merge.", default='')
@click.option('--delete/--keep', help="Delete merged columns.", default=False)
@click.option('--end-column', help="Column where the merge will be written.", default=None)
@click.argument('colname', nargs=-1, required=True)
@set_command
def cmd_merge_columns(i, colname, end_column, sep, delete, delimiter):
    return merge_columns(i, columns=colname, end_column=end_column, sep=sep, delete_columns=delete, delimiter=delimiter)

# TESTING BLOCK  TO REMOVE LATER
# @click.option('--after-counter', help="String between column index and column name.", default=': ')
# @click.option('--before-counter', help="String before column index.", default='')
# @click.option('--counter-start', help="Line number start.", default=1)
# @click.option('--count/--no-count', help="Numbers each line.", default=False)
# @click.option('--column-separator', help="Column separator.", default='\t')
# @set_commandx
# def cmd_get_col_names(*args, **kwargs):
#     return get_col_names(*args, **kwargs)

@click.option('--after-counter', help="String between column index and column name.", default=': ')
@click.option('--before-counter', help="String before column index.", default='')
@click.option('--counter-start', help="Line number start.", default=1)
@click.option('--count/--no-count', help="Numbers each line.", default=False)
@click.option('--output-delimiter', help="Column separator.", default='\t')
@set_command
def cmd_get_col_names(i, delimiter, count, counter_start, before_counter, after_counter, output_delimiter):
    return get_col_names(i, delimiter, count, counter_start, before_counter, after_counter, output_delimiter)

@click.argument('colname', nargs=-1, required=True)
@set_command
def cmd_rm_columns(i, delimiter, colname):
    return rm_columns(i, delimiter=delimiter, *colname)


@click.argument('colname')
@set_command
def cmd_rm_column(i, colname, delimiter):
    print "i: ", i
    return rm_column(i, colname, delimiter=delimiter)


@click.argument('colname')
@click.option('--col-index', help="Column index", default=0)
@set_command
def cmd_add_column(i, delimiter, colname, col_index=0):
    return add_column(i, colname, col_index, delimiter=delimiter)


@click.argument('colname_2')
@click.argument('colname_1')
@set_command
def cmd_swap_columns(i, colname_1, colname_2, delimiter):
    return swap_columns(i, colname_1, colname_2, delimiter=delimiter)


@click.argument('new_name')
@click.argument('old_name')
@set_command
def cmd_rename_column(i, old_name, new_name, delimiter):
    return rename_column(i, old_name, new_name, delimiter=delimiter)


@click.argument('colname', nargs=-1, required=True)
@set_command
def cmd_arrange_col_order(i, colname, delimiter):
    return arrange_col_order(i, colname, delimiter=delimiter)
