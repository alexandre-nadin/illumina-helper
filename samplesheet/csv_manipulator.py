import os, sys, csv
from StringIO import StringIO

CSV_DELIMITER = ','


def fill_column_with_str(csv_content, colname, value='', col_filters={}, delimiter=CSV_DELIMITER):
    """
    :param csv_content:
    :param colname:
    :param value:
    :param col_filters:
    :param delimiter:
    :return:
    """
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)

    """ Get correct lines."""
    valid_col_filters = [col_filter for col_filter in col_filters.keys() if col_filter in field_names]
    if (col_filters.__len__() > 0 and valid_col_filters.__len__() == 0) or colname not in field_names:
        return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()

    for dict_data in list_dict_data:
        to_modify = True
        for col_filter in valid_col_filters:
            """ Line must match all column values provided."""
            if dict_data[col_filter] not in col_filters[col_filter]:
                to_modify = False
                break
        if to_modify is True:
            dict_data[colname] = value

    return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()


def fill_column_with_serial(csv_content,  colname, stop, start=0, step=1, repeat=False, col_filters={}
                            , delimiter=CSV_DELIMITER):
    """
    :param csv_content:
    :param delimiter:
    :param colname:
    :param stop:
    :param start:
    :param step:
    :param repeat: cycles the numbers if not enough.
    :param col_filters:
    :return:
    """
    import math
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)
    numbers = range(start, stop, step)
    nb_cycling = int(math.ceil(float(list_dict_data.__len__())/numbers.__len__()))
    pool_to_insert = numbers * nb_cycling if repeat else numbers

    """ Get correct lines."""
    valid_col_filters = [col_filter for col_filter in col_filters.keys() if col_filter in field_names]
    if (col_filters.__len__() > 0 and valid_col_filters.__len__() == 0) or colname not in field_names:
        return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()

    for dict_data in list_dict_data:
        to_modify = True
        to_insert = pool_to_insert[:]
        for col_filter in valid_col_filters:
            """ Line must match all column values provided."""
            if dict_data[col_filter] not in col_filters[col_filter]:
                to_modify = False
                break
        if to_modify is True and to_insert.__len__() > 0:
            dict_data[colname] = pool_to_insert.pop(0)
    return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()


def duplicate_lines(csv_content, times=1, where={'': ()}, delimiter=CSV_DELIMITER):
    """
    Duplicates a line where one value from each specified column must match.
    :param csv_content:
    :param delimiter:
    :param times: Number of duplicates.
    :param where: Dictionary of column names and values.
    :return:
    """
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)

    """ Get correct lines."""
    valide_cols = [colname for colname in where.keys() if colname in field_names]
    if valide_cols.__len__() == 0:
        return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()

    new_list_dict_data = []
    for dict_data in list_dict_data:
        new_list_dict_data.append(dict_data)
        to_modify = True
        for colname in valide_cols:
            """ Line must match all column values provided."""
            if dict_data[colname] not in where[colname]:
                to_modify = False
                break
        if to_modify is True:
            for x in range(times):
                new_list_dict_data.append(dict_data)

    return write_dict_to_csv(StringIO(), new_list_dict_data, field_names, delimiter=delimiter).getvalue()


def sort_by_cols(csv_content, colnames=(), reverse=False, delimiter=CSV_DELIMITER):
    """
    Sorts csv content's lines by the specified column names.
    :param csv_content:
    :param delimiter:
    :param colnames:
    :param reverse: reverse the sorting order.
    :return:
    """
    import operator
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)

    colnames = [colname for colname in colnames if colname in field_names]
    if colnames.__len__() > 0:
        new_list_dict_data = sorted(list_dict_data, key=operator.itemgetter(*colnames), reverse=reverse)
        return write_dict_to_csv(StringIO(), new_list_dict_data, field_names, delimiter=delimiter).getvalue()
    return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()


def merge_columns(csv_content, columns=(), end_column=None, sep='', delete_columns=False, delimiter=CSV_DELIMITER):
    """
    Merges the specified columns in one single column. Respects the order in the column names list.
    :param csv_content:
    :param delimiter:
    :param sep: Separator between merged values.
    :param end_column: Where to merge the columns. By default it is the last column name specified.
    :param delete_columns: Deletes the merged columns
    :param columns:
    :return:
    """
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)
    end_column = columns[-1] if end_column is None else end_column
    """ Merge the columns."""
    if end_column in field_names:
        for dict_data in list_dict_data:
            dict_data[end_column] = sep.join(dict_data[k] for k in columns
                                             if k in field_names and dict_data[k].strip().__len__() > 0)
        """ Deletes the merged columns."""
        if delete_columns is True:
            return rm_columns(write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()
                              , *[col for col in columns if not col == end_column])

    return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()


def line_has_value(data_dict, key, value_searched):
    pass


def get_col_names(csv_content, delimiter=CSV_DELIMITER, count=False, counter_start=1, before_counter=''
                  , after_counter=': ', output_delimiter='\t'):
    """
    Lists all column names from a CSV formatted content with their index position.
    :param csv_content:
    :param delimiter:
    :param count: writes the index for each column.
    :param counter_start: number at which the index starts.
    :param before_counter: string before the column index.
    :param after_counter: string between the column index and the column name.
    :param output_delimiter: Separator between each column name.
    :return:
    """
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)
    res = ""
    for idx, val in enumerate(field_names):
        res += "{}{}{}{}{}".format(decode_unicode_escape(before_counter) if count else ''
                                   , str(idx + counter_start) if count else ''
                                   , decode_unicode_escape(after_counter) if count else ''
                                   , val
                                   , decode_unicode_escape(output_delimiter))
    return res


def rm_columns(csv_content, delimiter=CSV_DELIMITER, *names):
    for name in names:
        csv_content = rm_column(csv_content, delimiter=delimiter, name=name)
    return csv_content


def rm_column(csv_content, delimiter=CSV_DELIMITER, name=''):
    """
    Removes a column from a CSV content.
    :param csv_content:
    :param delimiter:
    :param name: name of column to remove.
    :return: a new csv formatted string.
    """
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)
    if name in field_names:
        field_names.pop(field_names.index(name))
        for dict_data in list_dict_data:
            if name in dict_data:
                dict_data.pop(name)
    return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()


def add_column(csv_content, name='', index=0, delimiter=CSV_DELIMITER):
    """
    Adds a new column to a CSV content if it does not already exist. If index is a string of out of bounds, column will
    be added at the end.
    :param csv_content:
    :param delimiter:
    :param name:
    :param index:
    :return: a new csv formatted string.
    """
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)
    index = field_names.__len__() if isinstance(index, str) \
            else field_names.__len__() if abs(index) > field_names.__len__() \
            else index

    if name not in field_names:
        field_names.insert(index, name)
        for dict_data in list_dict_data:
            if name not in dict_data:
                dict_data[name] = ""
    return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()


def swap_columns(csv_content, n1, n2, delimiter=CSV_DELIMITER):
    """
    Swaps two columns from a CSV formatted string.
    :param csv_content:
    :param delimiter:
    :param n1:
    :param n2:
    :return: a new CSV formatted string.
    """
    try:
        list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)
        i1, i2 = field_names.index(n1), field_names.index(n2)
        field_names[i1], field_names[i2] = field_names[i2], field_names[i1]
        return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()
    except ValueError:
        return csv_content


def rename_column(csv_content, old_name, new_name, delimiter=CSV_DELIMITER):
    """
    Renames a column from a CSV formatted string.
    :param csv_content: a CSV formatted string.
    :param delimiter:
    :param old_name: old column name.
    :param new_name: new column name.
    :return: a CSV formatted string.
    """
    list_dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)
    if old_name in field_names:
        field_names[field_names.index(old_name)] = new_name
        for dict_data in list_dict_data:
            dict_data[new_name] = dict_data.pop(old_name)
    return write_dict_to_csv(StringIO(), list_dict_data, field_names, delimiter=delimiter).getvalue()


def arrange_col_order(csv_content, colnames, delimiter=CSV_DELIMITER):
    """
    Reorganises the columns from a CSV formatted string.
    :param csv_content:
    :param delimiter:
    :param colnames:
    :return: a CSV formatted string.
    """
    dict_data, field_names = read_csv_to_dict(csv_content, delimiter=delimiter)

    """ Filter the correct column names"""
    filtered_colnames = tuple([x for x in colnames if x in field_names])

    """ We need to fill the remaining columns."""
    for f in field_names:
        if f not in filtered_colnames:
            filtered_colnames = filtered_colnames + (f,)
    return write_dict_to_csv(StringIO(), dict_data, filtered_colnames, delimiter=delimiter).getvalue()


def read_csv_to_dict(csv_file, delimiter=CSV_DELIMITER):
    """
    Reads a CSV formatted file/string.
    :param csv_file:
    :param delimiter:
    :return:
    """
    """ The delimiter must be a one char length string. """
    current_delimiter = str(decode_unicode_escape(delimiter))
    final_delimiter = current_delimiter if len(current_delimiter) == 1 else ','

    list_dict_data = []
    file_name = str(csv_file).strip('\n')
    if os.path.isfile(os.path.abspath(file_name)):
        with open(file_name) as f:
            reader = csv.DictReader(f, delimiter=final_delimiter)
            for row in reader:
                list_dict_data.append(row)

    else:
        reader = csv.DictReader(csv_file.splitlines(), delimiter=final_delimiter)
        for row in reader:
            list_dict_data.append(row)
    return list_dict_data, reader.fieldnames


def write_dict_to_csv(csv_file, dict_data, csv_columns, delimiter=CSV_DELIMITER):
    """
    Writes dictionary to csv format in a file, standard output or StringIO.
    :param csv_file: a file or standard output/error
    :param delimiter:
    :param csv_columns: The columns to list. The order is respected.
    :param dict_data: The dictionary
    :return:
    """

    """ The delimiter must be a one char length string. """
    current_delimiter = str(decode_unicode_escape(delimiter))
    final_delimiter = current_delimiter if len(current_delimiter) == 1 else ','

    try:
        if csv_file in [sys.stdout, sys.stderr] or isinstance(csv_file, StringIO):
            writer = csv.DictWriter(csv_file, fieldnames=csv_columns, delimiter=final_delimiter)
            writer.writeheader()
            writer.writerows(dict_data)
        else:
            with open(csv_file, 'w') as csv_file:
                writer = csv.DictWriter(csv_file, fieldnames=csv_columns, delimiter=final_delimiter)
                writer.writeheader()
                for data in dict_data:
                    writer.writerow(data)

    except IOError as (errno, strerror):
            print("I/O error({0}): {1}".format(errno, strerror))
    return csv_file


def decode_unicode_escape(string):
    """
    Decodes escape characters.
    :param string: The string to decode.
    :return:
    """
    import codecs
    try:
        return codecs.decode(string, "unicode_escape")
    except UnicodeDecodeError:
        return string


def runTests(argv=None):
    # path = "/Users/alexandre-nadin/dev/illumina-project-restructor/tests/lustre1/SampleSheets/"
    path = os.environ["DEV"] + "/tests/sample-sheets/"

    from sample_sheet import SampleSheet
    fileNames = [
        "test-no-metadata.csv"
        , "test-with-metadata-end.csv"
        , "test-with-metadata-between.csv"
        , "test-with-metadata-no-data.csv"
        , "test-with-metadata-between-emtpy.csv"
        , "old-with-empty-line-n-spaces.csv"
        , "new-with-empty-line-n-spaces.csv"
        , "new-with-missing-fields.csv"
        , "SampleSheet_semicolon.csv"
        , "SampleSheet.csv"

    ]

    # sheet = SampleSheet(path + fileNames[6])
    # sheet.print_data()
    # sheet.print_summary()

    nlines = 2
    sheet = SampleSheet(path + fileNames[-1])
    scontent = sheet.get_sample_sheet_content()
    print "\n[SampleSheet] ", sheet.sampleSheetPath, "\n", os.linesep.join(scontent.splitlines())

    order = get_col_names(scontent, count=True)
    print "\n[List Columns]\n", os.linesep.join(order.splitlines()[0:nlines])

    order = arrange_col_order(scontent, ["Sample_Project", "Sample_ID", "Sample_Name"])
    print "\n[Arrange] Put Sample_Project, Sample_ID, Sample_Name in front", os.linesep.join(
        order.splitlines()[0:nlines])

    order = arrange_col_order(order, ["index", "Sample_Well"])
    print "\n[Arrange] Put index, Sample_Well in front\n", os.linesep.join(order.splitlines()[0:nlines])

    order = rename_column(order, "Samppple_Well", "SampleWell")
    print "\n[Renaming] Samppple_Well to SampleWell\n", os.linesep.join(order.splitlines()[0:nlines])

    order = rename_column(order, "Sample_Well", "SampleWell")
    print "\n[Renaming] Sample_Well to SampleWell\n", os.linesep.join(order.splitlines()[0:nlines])

    order = swap_columns(order, "Sample_Well", "index2")
    print "\n[Swapping inexistent Sample_Well with index2]\n", os.linesep.join(order.splitlines()[0:nlines])

    order = swap_columns(order, "SampleWell", "index2")
    print "\n[Swapping SampleWell with index2]\n", os.linesep.join(order.splitlines()[0:nlines])
    # print sheet.get_sample_sheet_content().splitlines()

    order = add_column(order, "stringindex", "first col!")
    print "\n[Add column] stringindex\n", os.linesep.join(order.splitlines()[0:nlines])

    order = add_column(order, "idx50", 50)
    print "\n[Add column] idx50\n", os.linesep.join(order.splitlines()[0:nlines])

    order = add_column(order, "idx-50", -50)
    print "\n[Add column] idx-50\n", os.linesep.join(order.splitlines()[0:nlines])

    order = add_column(order, "idx-50", 2)
    print "\n[Add column] idx-50\n", os.linesep.join(order.splitlines()[0:nlines])

    order = add_column(order, "idx2", 2)
    print "\n[Add column] idx2\n", os.linesep.join(order.splitlines()[0:nlines])

    order = rm_column(order, "idx22")
    print "\n[Remove column] idx22\n", os.linesep.join(order.splitlines()[0:nlines])

    order = rm_column(order, "idx2")
    print "\n[Remove column] idx2\n", os.linesep.join(order.splitlines()[0:nlines])

    order = rm_columns(order, "stringindex", "idx50", "idx-50")
    print "\n[Remove columns] stringindex, idx50, idx-50, blabladodo\n", os.linesep.join(order.splitlines()[0:nlines])

    order = merge_columns(order, sep="-", end_column=None, delete_columns=False,
                          columns=["blabla", "index2", "index", "Sample_ID", "SampleWell", "bloubli"])
    print "\n[Merge columns] blabla, index2, ..., bloubli; nodeletion; no end column\n", os.linesep.join(
        order.splitlines()[0:nlines])

    order = merge_columns(order, sep="-", end_column=None, delete_columns=False,
                          columns=["blabla", "index2", "index", "Sample_ID", "SampleWell", "bloubli"])
    print "\n[Merge columns] blabla index2, index, .. bloubli ; nodeletion; no end column\n", os.linesep.join(
        order.splitlines()[0:nlines])

    order = merge_columns(order, sep="-", end_column=None, delete_columns=True,
                          columns=["blabla", "index2", "index", "Sample_ID", "SampleWell", "bloubli"])
    print "\n[Merge columns] blabla index2, index,  ... bloubli; delete cols; no end column\n", os.linesep.join(
        order.splitlines()[0:nlines])

    order = merge_columns(order, sep="-", end_column=None, delete_columns=True
                          , columns=["blabla", "index2", "index", "Sample_ID", "SampleWell"])
    print "\n[Merge columns] blabla index2, index, Sample_ID, SampleWell; delete cols; no end column\n", os.linesep.join(
        order.splitlines()[0:nlines])

    order = merge_columns(order, sep="-_-", end_column="Description", delete_columns=True
                          , columns=["SampleWell", "I5_Index_ID"])
    print "\n[Merge columns] SampleWell, I5_Index_ID; in Description; delete cols; \n", os.linesep.join(
        order.splitlines())

    order = sort_by_cols(order, colnames=("Description",))
    print "\n[Sort Columns] by Descritpion\n", os.linesep.join(order.splitlines())

    print "\n[Duplicate Lines] I7_Index_ID= \n", duplicate_lines(order, times=2, where={"I7_Index_ID": ["N704", "N703"]
        , 2: "second", "Description": ["TCCTGAGC-33713BA"]})

    print "\n[Duplicate Lines] I7_Index_ID= \n", duplicate_lines(order, times=2, where={"I7_Index_ID": ["N704", "N703"]
        , 2: "second", "Description": ["TCCTGAGC-33713BA", "AGAGTAGA-TCCTGAGC-33713BA-_-S504"]})

    print "\n---------\nRESET SAMPLE SHEET\n--------"
    print "\n[SampleSheet]\n", os.linesep.join(scontent.splitlines())

    order = duplicate_lines(scontent, times=5, where={"Sample_ID": ["5510BA", "PseudomonasF"]})
    print "\n[Duplicate Lines] where Sample_ID=[5510BA, PseudomonasF] \n", os.linesep.join(order.splitlines())

    print "\n[Fill column nb] 1-9  Sample_Plate\n" \
        , fill_column_with_serial(order, colname="Sample_Plate", stop=10, start=1, step=2,
                                  col_filters={"Sample_ID": ["5510BA"]})

    print "\n[Fill column nb] 1-100  Sample_Plate wrong I5_Index_ID \n" \
        , fill_column_with_serial(order, colname="Sample_Plate", stop=100, start=1, step=15,
                                  col_filters={"Sample_ID": ["PseudomonasF"], "I5_Index_ID": ["S5001"]})

    print "\n[Fill column nb] 1-100  Sample_Plate right I5_Index_ID\n" \
        , fill_column_with_serial(order, colname="Sample_Plate", stop=100, start=1, step=15,
                                  col_filters={"Sample_ID": ["PseudomonasF"], "I5_Index_ID": ["S501"]})

    print "\n[Fill column nb] 1-100 ; no filter\n" \
        , fill_column_with_serial(order, colname="Sample_Plate", stop=100, start=1, step=15)

    print "\n[Fill column string] blalbas  Sample_Plate right I5_Index_ID\n" \
        , fill_column_with_str(order, colname="Sample_Plate", value="blabla"
                               , col_filters={"Sample_ID": ["PseudomonasF"], "I5_Index_ID": ["S501"]})

    print "\n[Fill column string] blalbas ; Sample_Plate; no filter\n" \
        , fill_column_with_str(order, colname="Sample_Plate", value="blabla")

    print "\n[Colnames] from file ", fileNames[-2], "\n", get_col_names(path + fileNames[-2])

    # ----
    # Check ';' separator
    print "\n---------\nCheck ';' separator\n----------"
    sheet = SampleSheet(path + fileNames[-2])
    scontent = sheet.get_sample_sheet_content()
    print "\n[SampleSheet ';'] default separator; ", sheet.sampleSheetPath, "\n", os.linesep.join(scontent.splitlines())

    print "\n[List Columns]\n", get_col_names(scontent, delimiter=';', count=True)

    order = fill_column_with_str(scontent, colname="Sample_Plate", value="blabla")
    print "\n[Fill column string] blabla ; Sample_Plate; no filter; Should not work\n" \
        , os.linesep.join(order.splitlines())

    order = fill_column_with_str(scontent, colname="Sample_Plate", value="blabla", delimiter=';')
    print "\n[Fill column string] blabla ; Sample_Plate; no filter; Should work\n" \
        , os.linesep.join(order.splitlines())


def main(argv=None):
    runTests(argv)
    # cli()

if __name__ == "__main__":
    sys.exit(main(sys.argv))
