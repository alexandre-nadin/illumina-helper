__author__ = 'alexandre-nadin'

# -*- coding: utf-8 -*-

import os, sys, csv, re


class MetadataReader:

    METADATA_PATTERN_DEFAULT = r'\s*\[\s*((\w*\s*)+)\s*\]\s*$'
    MD_DELIMITERS_DEFAULT = ["[", "]"]
    MD_INNER_PATTERN_DEFAULT = '((\w*\s*)+)'

    DATA_KEY_VALUE_DELIMITER = "="
    DATA_VALUES_DELIMITER = ","
    COMMENT_CHAR = "#"

    def __init__(self, fileName, delimiters=MD_DELIMITERS_DEFAULT, ignoreCase=False):
        self._errors = []
        # print "\n[METADATA READER]"
        self._md_delimiters = self.checkMdDelimiters(delimiters)
        self._fileName = fileName
        self._allMetadata = {}
        self._ignoreCase = ignoreCase
        self._errors = []

        # print "\n[READING MD FILE] ", fileName
        try:
            with open(self._fileName, 'rU') as file:
                # print "open file: ", file

                mdRexpression = self.getMdPatternRe(self.MD_INNER_PATTERN_DEFAULT)
                currentMetadata = None
                for line in file:
                    line = str(line).strip(os.linesep).strip(" ")
                    if len(line) > 0 and not line.startswith(self.COMMENT_CHAR):
                        ## Check for metadata match
                        mdMatch = self.matchPattern(mdRexpression, line)
                        if mdMatch:
                            currentMetadata = self._getMdName(mdMatch)
                            # print "Metadata found: ." + str(mdMatch) + ". -> ." + str(currentMetadata) + "."

                            ## Set the dict if metadata doesn't exist yet.
                            if not self._allMetadata.has_key(currentMetadata):
                                self._allMetadata[currentMetadata] = []

                        ## Check for data content.
                        else:
                            if currentMetadata is not None:
                                self._allMetadata[currentMetadata].append(str(line).strip("\n").strip(" "))
        except IOError as e:
            print "\t-> ", e.strerror


    def printAllMetadata(self):
        # print "\n[METADATA READER] Printing metadata and content."
        for key in self._allMetadata.keys():
            print "\n" + key
            for data in self._allMetadata[key]:
                print "\t" + str(data)


    def _getMdName(self, metadata):
        """
        Strips a metadata from its delimiters and whitespaces.
        Assumes the two delimiters have already been checked.
        Assumes the metadata is a valid one, and comprises both its delimiters.
        Sets to upper case if ignoring the case.

        :param metadata:
        :return:
        """

        splitted = str(metadata).split(self._md_delimiters[0])
        if splitted.__len__() < 2: # Should have at least one delimiter
            return None
        elif splitted.__len__() > 3: # Should not have more than 2 delimiters, potentially identical.
            return None

        # Delimiters are identical. No need to split another time.
        elif splitted.__len__() == 3 and self._md_delimiters[0] == self._md_delimiters[1]:
            return splitted[1].strip(" ")

        # Exclude Two identical delimiters when two different expected
        elif splitted.__len__() == 3:
            return None

        else: # Len = 2, one delimiter has been split.
            splitted = splitted[1].split(self._md_delimiters[1]) # Get new split
            if splitted.__len__() != 2:
                return None
            return splitted[0].strip(" ").upper() if self._ignoreCase else splitted[0].strip(" ")


    def getMdPattern(self, mdInnerPattern=MD_INNER_PATTERN_DEFAULT):
        mdFirstPattern = '\s*' + '\\' + str(self._md_delimiters[0]) + "\s*" # Beginning of line
        mdLastPattern = '\s*\\' + self._md_delimiters[1] + '\s*'  # Ending of line
        return str(mdFirstPattern + mdInnerPattern + mdLastPattern)


    def getMdPatternRe(self, mdInnerPattern=MD_INNER_PATTERN_DEFAULT):
        """
        Build the regular expression pattern for any metadata
        :param mdInnerPattern:
        :return: the compiled regexp
        """
        return re.compile(self.getMdPattern(mdInnerPattern), re.IGNORECASE)


    def checkMdDelimiters(self, delimiters):
        """
        Checks and sets the metadata delimiters. There should be at least 1 delimiter, at most 2.
        If only one delimiter is provided, assumes the second delimiter is also the same.
        By default, delimiters will be brackets '[' and ']'.

        delimiters are patterns that surround your metadata. If delimiters are brackets, metadata should look like:
        "[meta data]". More generally, it should look like "pattern1 metadata pattern2"

        :param delimiters:
        :return: an array of two delimiters.
        """
        if delimiters is None or len(delimiters) == 0 or delimiters[0] is None or str(delimiters[0]).strip(" ").__len__() == 0:
            return MetadataReader.MD_DELIMITERS_DEFAULT[:]
        elif len(delimiters) < 2 or delimiters[1] is None or str(delimiters[1]).strip(" ").__len__() == 0:
            return [delimiters[0], delimiters[0]]
        else:
            return delimiters[:]


    def setMdDelimiters(self, delimiters):
        """
        Sets the delimiters after performing checks on them.

        :param delimiters:
        :return:
        """
        self._md_delimiters = self.checkMdDelimiters(delimiters)


    def hasMetadata(self):
        """
        Checks if metadata were found.
        :returns: True if at least one metadata was found. False otherwise.
        """
        if self._allMetadata.keys().__len__() == 0:
            return False
        return True


    def getRawDataFromMd(self, metadata):
        """
        Gets the data content corresponding to the specified metadata.
        No detection of structuring in the data.
        White spaces will be stripped for convenience.
        :param metadata: The metadata from which to retrieve the data content.
        :return:
        """
        if self.isMetadata(metadata) is False:
            return None
        key = str(metadata).strip(" ").upper() if self._ignoreCase else str(metadata).strip(" ")
        if not self._allMetadata.has_key(key):
            return None
        return self._allMetadata[key]


    def getAllKeyValuesFromMd(self, metadata, key_regex=None):
        """
        Assumes each line of data is structured with a key and associated values. Key and values are separated by a
        symbol. Values are separated by a different symbol. By default, a structured data is as follows:
          key = value_1, value_2, value_x.
        :param metadata:
        :return: A dict of key-values corresponding to the specified metadata.
        """
        if self.isMetadata(metadata) is False:
            return None
        keyValues = {}
        rawData = self._allMetadata[metadata]
        for data in rawData:
            key, values = self._getKeyValFromData(data, key_regex=key_regex)
            if key is None or values is None:
                self._errors.append("This data is not well structured: \"" + str(data) + "\"")
            else:
                keyValues[key] = values
        return keyValues


    def _getKeyValFromData(self, data, key_val_delim=str(DATA_KEY_VALUE_DELIMITER), val_delim=DATA_VALUES_DELIMITER\
                           , key_regex=None):
        """
        Fetches the key and values from data, which are expected to be separated by specific delimiters.
        Each field is stripped from whitespaces.
        It's possible to specify a regular expression to be matched with the key.
        :param data:
        :param key_val_delim:
        :param val_delim:
        :return: the key and values.
        """
        ## Check key-value separation
        fields = str(data).strip(" ").split(key_val_delim)
        if fields.__len__() != 2:
            return [None, None]

        ## Check key follows specified regex
        key = fields[0].strip(" ")
        if key_regex:
            key_match = key_regex.match(str(key))
            if not key_match:
                return [None, None]

        ## Check values
        values = []
        for value in fields[1].split(val_delim):
            values.append(value.strip(" "))
        return [key, values]


    def isMetadata(self, metadata):
        """
        Checks if a metadata is in the list of available metadata.
        :param metadata:
        :return:
        """
        if metadata is None or len(str(metadata).strip(" ")) == 0 or self.hasMetadata() is False\
                or metadata not in self._allMetadata.keys():
            return False
        return True


    def matchPattern(self, rexpression, content):
        match = rexpression.match(content)
        if not match:
            return None
        return match.group()


    def getAllMetadataDict(self):
        return self._allMetadata


    def reportErrors(self):
        if self._errors.__len__() > 0:
            print "\n[ERRORS]"
            for e in self._errors:
                print "*", e


def main(argv=None):
    # if argv is None or argv.__len__() < 2:
    #     print "Please specify a file to read metadata from."
    #     return None
    # mdr = MetadataReader(argv[1])

    sampleSheetsPath = "/Users/alexandre-nadin/dev/illumina-project-restructor/tests/sample-sheets/"
    sampleSheetName = "test-with-metadata-between.csv"
    mdr = MetadataReader(sampleSheetsPath + sampleSheetName)
    mdr.printAllMetadata()
    print "get content from \"alternative data\""
    print mdr.getRawDataFromMd("alternative dAtA  ")
    # test(path + "151104_SN859_0242_AHTKFYADXX_IEM.csv")


if __name__ == "__main__":
    sys.exit(main(sys.argv))

    # print mdr.getRawDataFromMd("ProjectConf")
    # print "[GET STRUCTURED]"
