from setuptools import setup, find_packages

setup(
    name='illumina-helper',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click',
    ],
    entry_points='''
        [console_scripts]
        csv.add-column=samplesheet.cli_commands:cmd_add_column
        csv.swap-columns=samplesheet.cli_commands:cmd_swap_columns
        csv.arrange-col-order=samplesheet.cli_commands:cmd_arrange_col_order
        csv.rename-column=samplesheet.cli_commands:cmd_rename_column
        csv.rm-column=samplesheet.cli_commands:cmd_rm_column
        csv.rm-columns=samplesheet.cli_commands:cmd_rm_columns
        csv.get-col-names=samplesheet.cli_commands:cmd_get_col_names
        csv.merge-columns=samplesheet.cli_commands:cmd_merge_columns
        csv.sort-by-cols=samplesheet.cli_commands:cmd_sort_by_cols
        csv.duplicate-lines=samplesheet.cli_commands:cmd_duplicate_lines
        csv.fill-column-with-serial=samplesheet.cli_commands:cmd_fill_column_with_serial
        csv.fill-column-with-str=samplesheet.cli_commands:cmd_fill_column_with_str
        samplesheet2casava=samplesheet.cli_commands:cmd_convert_to_casava
    ''',
)

